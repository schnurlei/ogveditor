import axios from 'axios';
import User from '@/store/auth/user';

const API_URL = 'api/auth/';

class AuthService {

  login (request: LoginRequest) {

    const requestObj = Object.assign({}, request);
    return axios.post(API_URL + 'signin', requestObj)
      .then(this.handleResponse)
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout () {
    localStorage.removeItem('user');
  }

  register (user: User) {
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    });
  }

  handleResponse (response: any) {
    if (response.status === 401) {
      this.logout();
      location.reload(true);

      const error = response.data && response.data.message;
      return Promise.reject(error);
    }

    return Promise.resolve(response);
  }
}

export default new AuthService();

export interface SignupRequest {

  nachname: string;
  email: string;
  password: string;
}

export interface LoginRequest {

  email: string;
  password: string;
}

export interface ResetPasswordRequest {

  resetId: string;
  password: string;
}
