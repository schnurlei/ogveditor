export default function authHeader () {

  const userJson: string | null = localStorage.getItem('user');
  const user = userJson ? JSON.parse(userJson) : null;

  if (user && user.accessToken) {
    return { Authorization: 'Bearer ' + user.accessToken };
  } else {
    return {};
  }
}
