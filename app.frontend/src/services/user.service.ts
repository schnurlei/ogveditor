import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8080/api/test/';

class UserService {

  public getPublicContent () {
    return axios.get(API_URL + 'all');
  }

  public getUserBoard () {
    return axios.get(API_URL + 'user', { headers: authHeader() })
      .catch(error => {
        this.convertError(error);
        throw error;
      });
  }

  public getModeratorBoard () {
    return axios.get(API_URL + 'mod', { headers: authHeader() });
  }

  public getAdminBoard () {
    return axios.get(API_URL + 'admin', { headers: authHeader() });
  }

  private convertError (errorToConvert: Error) {

    // @ts-ignore
    if (errorToConvert && errorToConvert.response && errorToConvert.response.data && errorToConvert.response.data.message) {

      // @ts-ignore
      errorToConvert.message = errorToConvert.response.data.message;
    }
  }
}

export default new UserService();
