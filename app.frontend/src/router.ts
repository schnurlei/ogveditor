import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './components/Home.vue';
import OgvEventEdit from '@/components/page/OgvEventEdit.vue';
import OgvNewsEdit from '@/components/page/OgvNewsEdit.vue';

Vue.use(VueRouter);

export const PARA_NEW_OBJECT = '_$create';

function convertIdFn (route: any) {

  return (route.params.itemId === '_$create')
    ? { itemId: null, createNew: true }
    : { itemId: route.params.itemId, createNew: false };
}

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/events',
    name: 'events',
    // lazy-loaded
    component: () => import('./components/page/OgvEvents.vue')
  },
  {
    path: '/news',
    name: 'news',
    // lazy-loaded
    component: () => import('./components/page/OgvNews.vue')
  },
  {
    path: '/ogvnews/:itemId',
    name: 'ogvnews',
    props: convertIdFn,
    // lazy-loaded
    component: OgvNewsEdit
  },
  {
    path: '/ogvevent/:itemId',
    name: 'ogvevent',
    props: convertIdFn,
    // lazy-loaded
    component: OgvEventEdit
  },
  {
    path: '/admin',
    name: 'admin',
    // lazy-loaded
    component: () => import('./views/BoardAdmin.vue')
  },
  {
    path: '/mod',
    name: 'moderator',
    // lazy-loaded
    component: () => import('./views/BoardModerator.vue')
  },
  {
    path: '/user',
    name: 'user',
    // lazy-loaded
    component: () => import('./views/BoardUser.vue')
  },
  {
    path: '/public',
    name: 'public',
    // lazy-loaded
    component: () => import('./views/BoardPublic.vue')
  },
  {
    path: '/profile',
    name: 'profile',
    // lazy-loaded
    component: () => import('./components/auth/OgvProfil.vue')
  },
  {
    path: '/confirm/:registrationId',
    component: () => import('./components/auth/OgvRegisterConfirmation.vue'),
    props: true
  },
  {
    path: '/login',
    component: () => import('./components/auth/OgvLogin.vue')
  },
  {
    path: '/register',
    component: () => import('./components/auth/OgvRegister.vue')
  },
  {
    path: '/requestPassword',
    component: () => import('./components/auth/OgvRequestPassword.vue')
  },
  {
    path: '/reset_password/:resetId',
    component: () => import('./components/auth/OgvResetPassword.vue'),
    props: true
  }

];

const router = new VueRouter({
  routes
});

export default router;
