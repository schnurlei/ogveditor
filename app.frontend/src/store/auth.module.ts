import AuthService from '../services/auth.service';
import User from '@/store/auth/user';

const userJson: string | null = localStorage.getItem('user');
const user: User| null = userJson ? JSON.parse(userJson) : null;
const initialState = user
  ? { status: { loggedIn: true }, user }
  : { status: {}, user: null };

export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    login ({ commit }: any, user: User) {
      return AuthService.login(user).then(
        user => {
          commit('loginSuccess', user);
          return Promise.resolve(user);
        },
        error => {
          commit('loginFailure');
          return Promise.reject(error.response.data);
        }
      );
    },
    logout ({ commit }: any) {
      AuthService.logout();
      commit('logout');
    },
    register ({ commit }: any, user: User) {
      return AuthService.register(user).then(
        response => {
          commit('registerSuccess');
          return Promise.resolve(response.data);
        },
        error => {
          commit('registerFailure');
          return Promise.reject(error.response.data);
        }
      );
    }
  },
  mutations: {
    loginSuccess (state: any, user: User) {
      state.status = { loggedIn: true };
      state.user = user;
    },
    loginFailure (state: any) {
      state.status = {};
      state.user = null;
    },
    logout (state: any) {
      state.status = {};
      state.user = null;
    },
    registerSuccess (state: any) {
      state.status = {};
    },
    registerFailure (state: any) {
      state.status = {};
    }
  }
};
