import OgvPlace from '@/store/page/ogvplace';

export default interface OgvEvent {

  id: string;

  title: string;

  dateStart: string;

  timeStart: string;

  imageSrc: string;

  markdownUrl: string;

  organizer: string;

  place: OgvPlace | null;
};
