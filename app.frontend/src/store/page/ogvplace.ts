export default interface OgvPlace {

  id: number;

  city: string;

  lat: number;

  lng: number;
};
