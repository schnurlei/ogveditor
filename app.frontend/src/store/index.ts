import Vue from 'vue';
import Vuex from 'vuex';
import { auth } from './auth.module';
import { page } from './page.module';
import { jdy } from './jdy.module';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth,
    page,
    jdy
  }
});
