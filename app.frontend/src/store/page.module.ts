import OgvEvent from '@/store/page/ogvevent';
import axios from 'axios';
import Vue from 'vue';

export const PAGE_API_URL = 'api/page/';

export const page = {
  namespaced: true,
  state: {
    events: new Array<OgvEvent>(0),
    images: new Array<OgvEvent>(0)
  },
  actions: {

    loadAllEvents ({ commit }: any): any {
      return axios.get(PAGE_API_URL + 'allEvents')
        .then(response => {
          if (response.data) {
            commit('setAllEvents', response.data);
          }
        });
    }
  },
  mutations: {
    // @ts-ignore
    setAllEvents (state, newEvents: OgvEvent[]) {
      Vue.set(state, 'events', newEvents);
    }
  }
};
