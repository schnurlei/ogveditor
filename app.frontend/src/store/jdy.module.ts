import { convertI18nEntriesInfoI18nJson, convertRepo2I18nEntries } from '@jdynameta/jdy-i18n';
import { plantShopMessages } from '@jdynameta/jdy-test';
import { i18n } from '../i18n';
import { JsonHttpObjectReader } from '@jdynameta/jdy-http';
import { JdyNamedJsonFilter } from './jdy-view';

export const PAGE_API_URL = 'api/page/';

export const jdy = {
  namespaced: true,
  state: {
    metaData: { // Repository read from Jpa
      repo: null,
      error: null,
      jdyI18n: []
    },
    jdyFilters: {
      allFilters: {

      }
    }
  },
  actions: {

    fetchMetadataRepository ({ commit }: any) {

      const metaReader = new JsonHttpObjectReader('/api/page/jdy/');
      return new Promise((resolve, reject) => {
        return metaReader.loadMetadataFromDb().then(repo => {
          const entries = convertRepo2I18nEntries(repo, plantShopMessages);
          commit('setMetaDataRepo', { repo: repo, jdyI18n: entries });
          const generatedMessages: any = convertI18nEntriesInfoI18nJson(entries);
          i18n.mergeLocaleMessage('de', generatedMessages.de);
          i18n.mergeLocaleMessage('en', generatedMessages.en);
          resolve();
        })
          .catch(data => {
            commit('setMetadataReadError', data);
            reject(data);
          });
      });
    },

    addFilter  ({ commit }: any, filterToAdd: JdyNamedJsonFilter) {
      commit('addFilter', filterToAdd);
    }
  },
  mutations: {

    // @ts-ignore
    setMetaDataRepo (state, repoHolder) {

      state.metaData = {
        repo: repoHolder.repo,
        error: null,
        jdyI18n: repoHolder.jdyI18n
      };
    },
    // @ts-ignore
    setJpaError (state, error) {

      state.metaData = {
        repo: null,
        error: error,
        jdyI18n: []
      };
    },
    // @ts-ignore
    addFilter (state, filterToAdd: JdyNamedJsonFilter) {

      let entityFilters = state.jdyFilters.allFilters[filterToAdd.entity];
      if (!entityFilters) {
        entityFilters = {};
        state.jdyFilters.allFilters[filterToAdd.entity] = entityFilters;
      }
      entityFilters[filterToAdd.name] = filterToAdd.expressionJson;
    }
  }
};
