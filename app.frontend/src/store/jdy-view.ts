import {
  ExpressionPrimitiveOperator,
  JdyAndExpression,
  JdyAttributeInfo, JdyAttributePath,
  JdyClassInfo,
  JdyClassInfoQuery, JdyEqualOperator, JdyFilterCreationException,
  JdyOperatorExpression,
  JdyPrimitiveAttributeInfo,
  JdyTypedValueObject,
  ObjectFilterExpression
} from '@jdynameta/jdy-base';
import { JsonCompactFileReader, JsonCompactFileWriter, Operation } from '@jdynameta/jdy-json';
import { createFilterRepository, FilterCreator, getDefaultFilterNameMapping } from '@jdynameta/jdy-meta';

export interface OperatorExprHolder {
  operator: ExpressionPrimitiveOperator | null;
  attribute: JdyAttributeInfo | null;
  value: any | null;
}

export function convertHolderToJdyQuery (filterExpressions: OperatorExprHolder[]): JdyAndExpression {

  const operatorExpr: ObjectFilterExpression[] = [];
  filterExpressions
    .forEach(exprHolder => {
      if (exprHolder.operator && exprHolder.attribute) {
        operatorExpr.push(new JdyOperatorExpression(exprHolder.operator, exprHolder.attribute, exprHolder.value));
      }
    });

  return new JdyAndExpression(operatorExpr);
}

export function findBooleanAttrs (classinfo: JdyClassInfo) {
  const allBooleanAttrs: any[] = [];
  if (classinfo) {
    classinfo.forEachAttr(attrInfo => {
      if (attrInfo.isPrimitive()) {
        const primType = attrInfo as JdyPrimitiveAttributeInfo;
        if (primType.getType().getType() === 'BOOLEAN') {
          allBooleanAttrs.push({ item: 'item.' + attrInfo.getInternalName(), attr: attrInfo.getInternalName() });
        }
      }
    });
  }
  return allBooleanAttrs;
}

export function findDateAttrs (classinfo: JdyClassInfo) {

  const allDateAttrs: any[] = [];
  if (classinfo) {
    classinfo.forEachAttr(attrInfo => {
      if (attrInfo.isPrimitive()) {
        const primType = attrInfo as JdyPrimitiveAttributeInfo;
        if (primType.getType().getType() === 'TIMESTAMP') {
          allDateAttrs.push({ item: 'item.' + attrInfo.getInternalName(), attr: attrInfo.getInternalName() });
        }
      }
    });
  }
  return allDateAttrs;
}

export class JdyNamedJsonFilter {

  private readonly _entity: string;
  private readonly _name: string;
  private readonly _expressionJson: string;

  public constructor (entity: string, name: string, expressionJson: string) {

    this._entity = entity;
    this._name = name;
    this._expressionJson = expressionJson;
  }

  public get entity (): string {
    return this._entity;
  }

  public get name (): string {
    return this._name;
  }

  public get expressionJson (): string {
    return this._expressionJson;
  }
}

export function singleObjectFilter (object: JdyTypedValueObject): JdyClassInfoQuery {

  const operatorExpr: ObjectFilterExpression[] = [];
  const equalOperator = new JdyEqualOperator(false);
  object.$typeInfo.forEachAttr(attrInfo => {

    if (attrInfo.isKey()) {
      if (attrInfo.isPrimitive()) {
        operatorExpr.push(new JdyOperatorExpression(equalOperator, attrInfo, object.val(attrInfo)));
      } else {
        throw new JdyFilterCreationException('ObjectRefence key not supported');
      }
    }
  });

  const andExpr = new JdyAndExpression(operatorExpr);
  return new JdyClassInfoQuery(object.$typeInfo, andExpr, []);
}

export function convertToJdyQuery (filterExpressions: OperatorExprHolder[], classInfo: JdyClassInfo, allSelectAttributes: JdyAttributePath[]) {

  const operatorExpr: ObjectFilterExpression[] = [];
  filterExpressions
    .forEach(exprHolder => {
      if (exprHolder.operator && exprHolder.attribute) {
        operatorExpr.push(new JdyOperatorExpression(exprHolder.operator, exprHolder.attribute, exprHolder.value));
      }
    });

  const andExpr = new JdyAndExpression(operatorExpr);
  return new JdyClassInfoQuery(classInfo, andExpr, allSelectAttributes);
}

export function convertQueryToJsonString (metaQuery: JdyClassInfoQuery): string {

  const filterCreator = new FilterCreator();
  const appQuery = filterCreator.convertMetaFilter2AppFilter(metaQuery);
  const jsonWriter = new JsonCompactFileWriter(getDefaultFilterNameMapping());

  if (appQuery) {
    const resultList = jsonWriter.writeObjectList([appQuery], Operation.INSERT, null);
    return JSON.stringify(resultList);
  } else {
    return '';
  }
}

export function convertJsonStringToQuery (filterJsonString: string, entityType: JdyClassInfo): JdyClassInfoQuery {

  const appQueryInfo: JdyClassInfo | null = createFilterRepository().getClassInfo('AppQuery');
  const filterObjectVo: JdyTypedValueObject[] = new JsonCompactFileReader(getDefaultFilterNameMapping()).readObjectList(JSON.parse(filterJsonString), appQueryInfo);
  const filterObject: JdyClassInfoQuery = new FilterCreator().convertAppFilter2MetaFilterForType(filterObjectVo[0], entityType);
  return filterObject;
}
