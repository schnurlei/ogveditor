import VueI18n from 'vue-i18n';
import Vue from 'vue';

Vue.use(VueI18n);

const messages = {
  en: {
    $vuetify: {
      dataIterator: {
        rowsPerPageText: 'Items per page:',
        pageText: '{0}-{1} of {2}'
      }
    },
    JdyFilterPanel: {
      Attribute: 'Attribute',
      Operator: 'Operator',
      Value: 'Value',
      Aktion: 'Action',
      AddExpression: 'Add Expression',
      SelectAttribute: 'Select Attribut',
      SelectOperator: 'Select Operator',
      Save: 'Save',
      Cancel: 'Cancel',
      NoData: 'No expression defined'
    }
  },
  de: {
    $vuetify: {
      dataIterator: {
        rowsPerPageText: 'Element je Seite:',
        pageText: '{0}-{1} von {2}'
      }
    },
    JdyFilterPanel: {
      Attribute: 'Attribut',
      Operator: 'Operator',
      Value: 'Wert',
      Aktion: 'Aktion',
      AddExpression: 'Ausdruck erstellen',
      SelectAttribute: 'Attribut auswählen',
      SelectOperator: 'Operator auswählen',
      Save: 'Speichern',
      Cancel: 'Abbrechen',
      NoData: 'Kein Ausdruck erstellt'
    }
  }
};

export const i18n = new VueI18n({
  locale: 'en', // set locale
  messages // set locale messages
});
