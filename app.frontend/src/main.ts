// https://bezkoder.com/jwt-vue-vuex-authentication/
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Vuetify from 'vuetify/lib';
import VueI18n from 'vue-i18n';
import colors from 'vuetify/lib/util/colors';
import { i18n } from './i18n';
import JdyPrimitiveComponentHolder from '@/components/jdy/JdyPrimitiveComponentHolder.vue';
import JdyStringField from '@/components/jdy/JdyStringField.vue';
import JdyLongField from '@/components/jdy/JdyLongField.vue';
import JdyDecimalField from '@/components/jdy/JdyDecimalField.vue';
import JdyTimestampField from '@/components/jdy/JdyTimestampField.vue';
import JdyBooleanField from '@/components/jdy/JdyBooleanField.vue';
import JdyTextField from '@/components/jdy/JdyTextField.vue';

Vue.component('jdy-primitive', JdyPrimitiveComponentHolder);
Vue.component('jdy-string', JdyStringField);
Vue.component('jdy-long', JdyLongField);
Vue.component('jdy-decimal', JdyDecimalField);
Vue.component('jdy-timestamp', JdyTimestampField);
Vue.component('jdy-boolean', JdyBooleanField);
Vue.component('jdy-text', JdyTextField);

const vuetify = new Vuetify({
  theme: {
    themes: {
      light: {
        primary: colors.green.darken3
      }
    }
  }
});

Vue.config.productionTip = false;
Vue.use(Vuetify);
Vue.use(VueI18n);

new Vue({
  router,
  store,
  created () {
    store.dispatch('jdy/fetchMetadataRepository');
  },
  // @ts-ignore
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app');
