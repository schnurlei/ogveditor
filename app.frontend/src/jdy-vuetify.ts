import {
  JdyBooleanType,
  JdyClassInfo,
  JdyDecimalType, JdyFloatType,
  JdyPrimitiveAttributeInfo,
  JdyPrimitiveTypeVisitor, JdyTimeStampType
} from '@jdynameta/jdy-base';
import VueI18n, { IVueI18n } from 'vue-i18n';

export function convertToColumns (aClassInfo: JdyClassInfo, $i18n: VueI18n & IVueI18n) {
  const allColumns: any[] = [];
  aClassInfo.forEachAttr(attrInfo => {
    if (attrInfo.isPrimitive()) {
      const primitiveInfo = attrInfo as JdyPrimitiveAttributeInfo;
      const newCol = primitiveInfo.getType().handlePrimitiveKey(primitiveTypeToColumnHandler(attrInfo));
      if (newCol) {
        newCol.text = $i18n.t(attrInfo.i18n);
        allColumns.push(newCol);
      }
    }
  });
  return allColumns;
}

function primitiveTypeToColumnHandler (attrInfo: any): JdyPrimitiveTypeVisitor {
  return {
    // @ts-ignore
    handleBoolean: function (aType: JdyBooleanType) {
      return {
        text: attrInfo.getInternalName(),
        align: 'left',
        value: attrInfo.getInternalName(),
        left: true
      };
    },
    // @ts-ignore
    handleDecimal: function (aType: JdyDecimalType) {
      return {
        text: attrInfo.getInternalName(),
        align: 'left',
        value: attrInfo.getInternalName()
      };
    },
    // @ts-ignore
    handleTimeStamp: function (aType: JdyTimeStampType) {
      return {
        text: attrInfo.getInternalName(),
        align: 'left',
        value: attrInfo.getInternalName()
      };
    },
    // @ts-ignore
    handleFloat: function (aType: JdyFloatType) {
      return {
        text: attrInfo.getInternalName(),
        align: 'left',
        value: attrInfo.getInternalName()
      };
    },
    // @ts-ignore
    handleLong: function (aType: any) {
      return {
        text: attrInfo.getInternalName(),
        align: 'left',
        value: attrInfo.getInternalName()
      };
    },
    // @ts-ignore
    handleText: function (aType: any) {
      return {
        text: attrInfo.getInternalName(),
        align: 'left',
        value: attrInfo.getInternalName(),
        left: true
      };
    },

    handleVarChar: function (aType: any) {
      return null;
    },

    handleBlob: function (aType: any) {
      return null;
    }
  };
}
