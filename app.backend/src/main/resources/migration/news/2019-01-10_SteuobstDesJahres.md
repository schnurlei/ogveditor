Der Landesverband für Obstbau, Garten und Landschaft Baden-Württemberg e.V. (LOGL) hat den Öhringer Blutstreifling
zur Streuobstsorte des Jahres 2019.
Der wohlschmeckende, feste und saftige kann als Apfel Tafel-, Saft - und Mostapfel verwendet werden.

[Infoblatt](https://www.logl-bw.de/images/1_logl/streuobst/Streuobstsorte_des_Jahres/2019/Infoblatt_A4_Streuobstsorte_2019_%C3%96hringer_Blutstreifling.pdf)

Bild: Joergens.mi/Wikipedia



[original file](https://commons.wikimedia.org/wiki/File:%C3%96hringer_Blutstreifling_jm55056.jpg)
[creative commons license](https://creativecommons.org/licenses/by-sa/3.0/de/legalcode) 


