Wer selber Saatgut gewinnen möchte, sollte darauf achten, dass er keine F1-Hybiden als Saatgut kauft,
sondern samenfeste Sorten. 


Lieferanten für ökologische Saatgut sind:


[Bingenheimer Saatgut](https://www.bingenheimersaatgut.de/)

[Dreschflegel Bio-Saatgut](https://www.dreschflegel-saatgut.de/)



Samen für einheimische Wildblumen findet man unter:

[Rieger Hofman](www.rieger-hofmann.de/home.html)


Auch beim Illertisser Saatgutmarkt finder man viele Informationen rund um unser Saatgut.

[Illertisser Saatgutmarkt](https://www.gaissmayer.de/veranstaltungen/illertisser-saatgutmarkt-2019/)  