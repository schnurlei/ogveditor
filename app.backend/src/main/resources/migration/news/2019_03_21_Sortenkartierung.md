Im vergangenen Jahr wurden im Landkreis Neu-Ulm alte und seltene Apfelssorten kartiert.
Auch in unserer Gemeinde war Herr Bosch, der zuständige Apfelexperte, unterwegs.

Es hat Spaß gemacht, ihn dabei zu begleiten. Ich war begeistert, 
mit welchem Sachverstand und mit welcher Begeisterung er bei der Sache war.


In der folgenden Karte sind die im Rahmen des Projekts erfaßten Apfelbäume eingezeichnet:

[Sortenkartierung Nordschwaben Karte](https://schlaraffenburger.de/cms/index.php/sortenkartierung-nordschwaben-2016-map)         