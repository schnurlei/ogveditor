Folgende Institutionen bieten Kurse für Hobbygärner an:

[Hochschule Weihenstephan-Triesdorf](https://www.hswt.de/weihenstephaner-gaerten/freizeitgartenbau.html)

[Lehr- und Beispielsbetrieb für Obstbau Deutenkofen](https://www.obstbau-deutenkofen.de/kurse/fuer-hobbygaertner/)

[Bayerische Gartenakademie](https://www.lwg.bayern.de/gartenakademie/)

[Bildungszentrum Triesdorf](https://www.triesdorf.de/bildung-veranstaltungen/obstbau.html)

[Landesanstalt für Bienenkunde Uni Hohenheim](https://bienenkunde.uni-hohenheim.de/)

[Imkerschule Schwaben](www.imker-schwaben.de/imkerschule-schwaben.html)