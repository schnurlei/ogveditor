Mit "VIELFALTSMACHER - (G)ARTEN.REICH.NATUR" ist ein Projekt initiiert wordem, mit dem wir Menschen dafür begeistern möchten, ihren Garten zu einem Reich der Artenvielfalt und der Natur zu machen. 

Auf der Hompage [www.vielfaltsmacher.de](https://www.vielfaltsmacher.de) finden Sie viele Ideen und Anregungen, wie Sie Ihren Garten vielfätiger für Mensch und Natur gestalten können.

Das sind zum Beispiel Anleitungen zum Ansäen einer Blumenwiese oder von Blühfl  ächen im Garten. Es gibt Listen von insekten-freundlichen Stauden und Gehölzen. Wir zeigen, wie ein Gemüsegarten vielfältig wird, was einen insekten-freundlichen Balkon ausmacht und wir erklären ein paar grundsätzliche Dinge zur Vielfalt und zum Gärtnern. 


Weitere Informationen finden Sie unter

https://www.gartenbauvereine.org/wp-content/uploads/2020/01/ID-102-Vielfaltsmacher-Sonderausgabe.pdf
