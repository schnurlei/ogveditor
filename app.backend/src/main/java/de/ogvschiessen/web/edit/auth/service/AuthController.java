package de.ogvschiessen.web.edit.auth.service;

import de.ogvschiessen.web.edit.auth.model.ERole;
import de.ogvschiessen.web.edit.auth.model.OgvRole;
import de.ogvschiessen.web.edit.auth.model.OgvUser;
import de.ogvschiessen.web.edit.payload.request.LoginRequest;
import de.ogvschiessen.web.edit.payload.request.ResetPasswordRequest;
import de.ogvschiessen.web.edit.payload.request.SignupRequest;
import de.ogvschiessen.web.edit.payload.response.JwtResponse;
import de.ogvschiessen.web.edit.payload.response.MessageResponse;
import de.ogvschiessen.web.edit.auth.repository.OgvUserRepository;
import de.ogvschiessen.web.edit.auth.repository.RoleRepository;
import de.ogvschiessen.web.edit.service.JwtUtils;
import de.ogvschiessen.web.edit.common.service.MailService;
import de.ogvschiessen.web.edit.service.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.core.Response;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static de.ogvschiessen.web.edit.common.exception.JaxRsHandledException.jaxRsHandled;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private static final Logger LOG = Logger.getLogger(AuthController.class.getName());

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    OgvUserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private MailService mailService;

    @Value( "${ogveditor.host}" )
    private String hostUrl;

    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String jwt = jwtUtils.generateJwtToken(authentication);

        final UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        final List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getNachname(),
                roles));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {

            return ResponseEntity.status(HttpStatus.CONFLICT).body(new MessageResponse("Error: Email is already in use!"));
        }

        final byte[] registrationBytes = createSecureRandom();
        String registrationId = Base64.getUrlEncoder().encodeToString(registrationBytes);

        // Create new user's account
        OgvUser user = new OgvUser();
        user.setNachname(signUpRequest.getNachname());
        user.setEmail(signUpRequest.getEmail());
        user.setPassword(this.encoder.encode(signUpRequest.getPassword()));
        user.setRegistrationConfirmed(false);
        user.setRegistrationId(registrationId);

        OgvRole userRole = roleRepository.findByName(ERole.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        user.setRoles(Collections.singleton(userRole));
        this.userRepository.save(user);

        mailService.sendRegistrationMail(signUpRequest.getEmail(), registrationId, this.hostUrl);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @PutMapping(path = "/confirmRegistration/{registration-id}", produces = APPLICATION_JSON_VALUE)
    public Response confirmRegistration(final @PathVariable("registration-id") String registrationId){

        final OgvUser user = this.userRepository.findByRegistrationId(registrationId)
                .orElseThrow(() -> jaxRsHandled(HttpStatus.NOT_FOUND).msg("Registration id not found"));
        user.setRegistrationConfirmed(true);
        user.setRegistrationId(null);
        this.userRepository.save(user);
        return Response.ok().build();
    }

    @PutMapping(path = "/resetPassword", produces = APPLICATION_JSON_VALUE)
    public Response resetPassword(@Valid @RequestBody ResetPasswordRequest resetRequest) {

        final OgvUser user = this.userRepository.findByPasswordResetId(resetRequest.getResetId())
                .orElseThrow(() -> jaxRsHandled(HttpStatus.NOT_FOUND).msg("Email not found"));
        user.setPassword(encoder.encode(resetRequest.getPassword()));
        user.setPasswordResetId(null);
        this.userRepository.save(user);
        return Response.ok().build();
    }

    @PutMapping(path = "/requestPassword/{email}", produces = APPLICATION_JSON_VALUE)
    public Response requestPasswordReset(@PathVariable("email") String anEmail) {

        final OgvUser user = this.userRepository.findByEmail(anEmail)
                .orElseThrow(() -> jaxRsHandled(HttpStatus.NOT_FOUND).msg("Email not found"));
        String resetIdString = Base64.getUrlEncoder().encodeToString(createSecureRandom());
        user.setPasswordResetId(resetIdString);
        user.setPassword("<Invalid>");
        this.userRepository.save(user);
        this.mailService.sendPasswordResetMail(anEmail, resetIdString, this.hostUrl);
        return Response.ok().build();
    }

    private byte[] createSecureRandom() {

        Random ranGen = new SecureRandom();
        byte[] aesKey = new byte[32];
        ranGen.nextBytes(aesKey);
        return aesKey;
    }
}
