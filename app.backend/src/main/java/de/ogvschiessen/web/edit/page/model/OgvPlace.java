package de.ogvschiessen.web.edit.page.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Entity
public class OgvPlace {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max=120)
    @Column(length=120)
    @NotBlank
    private String city;

    @DecimalMin("-90.00000")
    @DecimalMax("90.00000")
    @Column(precision=8, scale=6)
    @NotNull
    BigDecimal lat;

    @DecimalMin("-180.00000")
    @DecimalMax("180.00000")
    @Column(precision=9, scale=6)
    @NotNull
    BigDecimal lng;

    public Long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLng() {
        return lng;
    }

    public void setLng(BigDecimal lng) {
        this.lng = lng;
    }
}
