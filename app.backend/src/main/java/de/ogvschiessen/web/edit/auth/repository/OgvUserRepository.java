package de.ogvschiessen.web.edit.auth.repository;

import de.ogvschiessen.web.edit.auth.model.OgvUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OgvUserRepository extends JpaRepository<OgvUser, Long> {

	Optional<OgvUser> findByRegistrationId(String aRegisterId);

	Optional<OgvUser> findByPasswordResetId(String aPasswordResetId);

	Optional<OgvUser> findByEmail(String username);

	Boolean existsByEmail(String email);
}
