package de.ogvschiessen.web.edit.auth.repository;

import de.ogvschiessen.web.edit.auth.model.ERole;
import de.ogvschiessen.web.edit.auth.model.OgvRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface RoleRepository extends JpaRepository<OgvRole, Integer> {
	
	Optional<OgvRole> findByName(ERole name);
}
