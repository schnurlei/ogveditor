/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ogvschiessen.web.edit.common.exception;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 *
 * @author rainer
 */
@XmlRootElement
public class ErrorResponse {
   
    private String i18nKey;
    private String plainMessage;
    private List<ValidationField> validations;

    public ErrorResponse() {
    }

    public ErrorResponse(String aPlainMessage) {
        this.plainMessage = aPlainMessage;
    }

    
    public ErrorResponse(JaxRsHandledException anException) {
        this.i18nKey = anException.getI18nKey();
        this.plainMessage = anException.getPlainMessage();
        this.validations = anException.getInvalidFields();
    }

    
    public String getI18nKey() {
        return i18nKey;
    }

    public void setI18nKey(String i18nKey) {
        this.i18nKey = i18nKey;
    }

    public String getPlainMessage() {
        return plainMessage;
    }

    public void setPlainMessage(String plainMessage) {
        this.plainMessage = plainMessage;
    }

    @XmlElement(name="validations")
    public List<ValidationField> getValidations() {
        return validations;
    }

    public void setValidations(List<ValidationField> validations) {
        this.validations = validations;
    }

    @XmlRootElement
    public static class ValidationField
    {
        private String invalidValue;
        private String message;
        private String path;

        public ValidationField()
        {
            
        }   
        
        ValidationField(ConstraintViolation<?> aValidation) {
            
            this.invalidValue = (aValidation.getInvalidValue() != null) ? aValidation.getInvalidValue().toString() : null;
            String propertyPath = "";
            for (Path.Node node : aValidation.getPropertyPath()) {
                propertyPath += node.getName()+".";
            }
            
            this.path = aValidation.getPropertyPath().toString();
            this.message = aValidation.getMessage();
        }

        public String getInvalidValue() {
            return invalidValue;
        }

        public String getMessage() {
            return message;
        }

        public void setInvalidValue(String invalidValue) {
            this.invalidValue = invalidValue;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        
    }
    
}
