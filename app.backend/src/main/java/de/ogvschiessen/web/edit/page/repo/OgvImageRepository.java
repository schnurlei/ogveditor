package de.ogvschiessen.web.edit.page.repo;

import de.ogvschiessen.web.edit.page.model.OgvImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface OgvImageRepository extends JpaRepository<OgvImage, Long> {
	
	Optional<OgvImage> findById(Long id);
}
