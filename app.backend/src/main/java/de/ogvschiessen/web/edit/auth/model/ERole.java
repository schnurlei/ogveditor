package de.ogvschiessen.web.edit.auth.model;

public enum ERole {
	ROLE_USER,
	ROLE_MODERATOR,
	ROLE_ADMIN
}
