
package de.ogvschiessen.web.edit.common.service;

import de.ogvschiessen.web.edit.common.exception.JaxRsHandledException;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.log.SystemLogChute;
import org.apache.velocity.runtime.resource.loader.StringResourceLoader;
import org.apache.velocity.runtime.resource.util.StringResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.NamingException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rainer
 */
@Component
public class MailService {

    private static final Logger LOG = Logger.getLogger(MailService.class.getName());

    protected static final String HTML_UTF8_CONTENT_TYPE = "text/html; charset=utf-8";

    @Autowired
    public JavaMailSender emailSender;

    public void sendPasswordResetMail(String anEmail, String resetIdString, String baseUri) {
       try {
            VelocityContext context = new VelocityContext();
            context.put("email", anEmail);
            context.put("url", createPasswordResetLink(resetIdString,baseUri));
            String mailText = createMailText(context, "password_reset_template.html");
            sendMailForRegistration(anEmail,"Spatentaten Passwort zurücksetzen", mailText);
        } catch (MessagingException | UnsupportedEncodingException | NamingException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw JaxRsHandledException.jaxRsHandled(HttpStatus.SERVICE_UNAVAILABLE).msg("Fehler beim Senden der Mail");
        } catch (URISyntaxException | IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw JaxRsHandledException.jaxRsHandled(HttpStatus.SERVICE_UNAVAILABLE).msg("Fehler beim Senden der Mail");
        }
     }
    
    public void sendRegistrationMail(String eMail, String registrationId, String baseUri)
    {
        try {
            VelocityContext context = new VelocityContext();
            context.put("email", eMail);
            context.put("url", createRegistrationLink(registrationId,baseUri));
            String mailText = createMailText(context, "email_registraion_template.html");
            sendMailForRegistration(eMail,"Spatentaten Registration", mailText);
        } catch (MessagingException | UnsupportedEncodingException | NamingException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw JaxRsHandledException.jaxRsHandled(HttpStatus.SERVICE_UNAVAILABLE).msg("Fehler beim Senden der Mail");
        } catch (URISyntaxException | IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw JaxRsHandledException.jaxRsHandled(HttpStatus.SERVICE_UNAVAILABLE).msg("Fehler beim Senden der Mail");
        }
    }
    
    public void sendMailForRegistration(String eMail,String subject, String mailText) throws MessagingException, UnsupportedEncodingException, NamingException, IOException, URISyntaxException {
        

        // Create email and headers.
        MimeMessage msg = this.emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setTo(eMail);
        helper.setSubject(subject);
        helper.setCc(eMail);
        helper.setFrom("admin@ogv-schiessen.de");

        Multipart multipart = new MimeMultipart();

        final MimeBodyPart htmlBodyPart = new MimeBodyPart();
        htmlBodyPart.setContent(mailText, HTML_UTF8_CONTENT_TYPE);
        multipart.addBodyPart(htmlBodyPart);

        // mailtext as attachment
        MimeBodyPart anhangPart = new MimeBodyPart();
        anhangPart.setFileName("mailText.html");
        anhangPart.setContent(mailText, "text/html");
        multipart.addBodyPart(anhangPart);

        msg.setContent(multipart);
        emailSender.send(msg);
    }
        
    String createMailText(VelocityContext context, String templateName) throws IOException, URISyntaxException 
    {        
       StringWriter out = new StringWriter();
        Template template = createTemplate(templateName);
        template.merge(context, out);
        return out.toString();
    }
    
    private Template createTemplate(String templateName) throws URISyntaxException, IOException, ParseErrorException, ResourceNotFoundException
    {
        VelocityEngine engine = createStringEngine();
        StringResourceRepository repo = (StringResourceRepository)engine.getApplicationAttribute(StringResourceLoader.REPOSITORY_NAME_DEFAULT);
        repo.putStringResource(templateName, getTemplateFromResource(templateName));
        Template template = engine.getTemplate(templateName);
        return template;
    }
    
   protected VelocityEngine createStringEngine()
    {
        VelocityEngine engine = new VelocityEngine();
        engine.setProperty(Velocity.RESOURCE_LOADER, "string");
        engine.addProperty("string.resource.loader.class", StringResourceLoader.class.getName());
        engine.addProperty("string.resource.loader.repository.static", "false");
        engine.addProperty("string.resource.loader.modificationCheckInterval", "1");
        engine.setProperty(Velocity.RUNTIME_LOG_LOGSYSTEM_CLASS, SystemLogChute.class.getName());
        engine.init();
        return engine;
    }

    
    Template getTemplate(final String templatePath) throws IOException, URISyntaxException {
	if (!Velocity.resourceExists(templatePath)) {
		StringResourceRepository repo = StringResourceLoader.getRepository();
		repo.putStringResource(templatePath, getTemplateFromResource(templatePath));
	}
	return Velocity.getTemplate(templatePath);
    }
    
    String getTemplateFromResource(final String templatePath) throws IOException, URISyntaxException {
        URL streamUrl = MailService.class.getResource(templatePath);
        return new String(Files.readAllBytes(Paths.get(streamUrl.toURI())),StandardCharsets.UTF_8);
    }
    
     
    protected String createRegistrationLink(String registrationId, String baseUri) {
        
        final StringBuilder registrationLinkBuilder = new StringBuilder();
        registrationLinkBuilder.append(baseUri).append("/#/confirm/");
        registrationLinkBuilder.append(registrationId);
        return registrationLinkBuilder.toString();
    }

    protected String createPasswordResetLink(String resetId, String baseUri) {
        
        final StringBuilder registrationLinkBuilder = new StringBuilder();
        registrationLinkBuilder.append(baseUri).append("/#/reset_password/");
        registrationLinkBuilder.append(resetId);
        return registrationLinkBuilder.toString();
    }

}
