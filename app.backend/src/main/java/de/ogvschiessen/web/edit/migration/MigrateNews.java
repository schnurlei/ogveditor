package de.ogvschiessen.web.edit.migration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.ogvschiessen.web.edit.common.service.MailService;
import de.ogvschiessen.web.edit.page.model.OgvNews;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MigrateNews {

    public List<OgvNews> readNews() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        URL streamUrl = MailService.class.getResource("/migration/news/news.json");
        JsonNode rootNode = mapper.readValue(streamUrl, JsonNode.class);
        List<OgvNews> allNews = new ArrayList<>();
        if (rootNode.isArray()) {

            Iterator<JsonNode> newsIter = rootNode.iterator();
            while(newsIter.hasNext()) {
                JsonNode curNode = newsIter.next();
                if (curNode.isObject()) {
                    OgvNews newsObj = new OgvNews();
                    String id = curNode.get("id").asText();
                    String newsDate = curNode.get("newsDate").asText();
                    String newsTime = curNode.get("newsTime").asText();
                    LocalDate date = LocalDate.parse(newsDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                    LocalTime time = LocalTime.parse(newsTime, DateTimeFormatter.ISO_TIME);
                    System.out.println(time);
                    newsObj.setNewsDate(date);
                    newsObj.setNewsTime(time);
                    newsObj.setSubject(curNode.get("subject").asText());
                    newsObj.setTitle(curNode.get("title").asText());
                    newsObj.setAbstractText(curNode.get("abstract").asText());
                    newsObj.setAbstractText(curNode.get("newsDate").asText());
                    newsObj.setImageSrc(curNode.get("imageSrc").asText());
                    newsObj.setMarkdownUrl(curNode.get("markdownUrl").asText());
                    if (curNode.get("link") != null) {
                        newsObj.setLink(curNode.get("link").asText());
                    }
                    allNews.add(newsObj);
                }
            }
        }
        return allNews;
    }

    public static void main(String[] args) throws IOException {
        new MigrateNews().readNews();
    }
}
