package de.ogvschiessen.web.edit.page.repo;

import de.ogvschiessen.web.edit.page.model.OgvEvent;
import de.ogvschiessen.web.edit.page.model.OgvNews;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface OgvNewsRepository extends JpaRepository<OgvNews, String> {
	
	Optional<OgvNews> findById(String id);
}
