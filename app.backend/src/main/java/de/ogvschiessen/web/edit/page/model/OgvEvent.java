package de.ogvschiessen.web.edit.page.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
public class OgvEvent {

        @Id
        @Size(max=50)
        private String id;

        @Size(max=120)
        @Column(length=120)
        @NotNull
        private String title;

        @NotNull
        private LocalDate dateStart;

        @NotNull
        private LocalTime timeStart;

        @Size(max=255)
        private String imageSrc;

        @Size(max=255)
        private String markdownUrl;

        @Size(max=20)
        @NotNull
        private String organizer;

        @OneToOne
        OgvPlace place;

        public String getId() {
                return id;
        }

        public void setId(String id) {
                this.id = id;
        }

        public String getTitle() {
                return title;
        }

        public void setTitle(String title) {
                this.title = title;
        }

        public LocalDate getDateStart() {
                return dateStart;
        }

        public void setDateStart(LocalDate dateStart) {
                this.dateStart = dateStart;
        }

        public LocalTime getTimeStart() {
                return timeStart;
        }

        public void setTimeStart(LocalTime timeStart) {
                this.timeStart = timeStart;
        }

        public String getImageSrc() {
                return imageSrc;
        }

        public void setImageSrc(String imageSrc) {
                this.imageSrc = imageSrc;
        }

        public String getMarkdownUrl() {
                return markdownUrl;
        }

        public void setMarkdownUrl(String markdownUrl) {
                this.markdownUrl = markdownUrl;
        }

        public String getOrganizer() {
                return organizer;
        }

        public void setOrganizer(String organizer) {
                this.organizer = organizer;
        }

        public OgvPlace getPlace() {
                return place;
        }

        public void setPlace(OgvPlace place) {
                this.place = place;
        }
}
