package de.ogvschiessen.web.edit.page.repo;

import de.ogvschiessen.web.edit.page.model.OgvPlace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface OgvPlaceRepository extends JpaRepository<OgvPlace, Long> {
	
	Optional<OgvPlace> findById(String id);
}
