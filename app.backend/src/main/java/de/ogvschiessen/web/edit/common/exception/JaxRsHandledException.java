/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.ogvschiessen.web.edit.common.exception;

import org.springframework.http.HttpStatus;

import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rainer
 */
public class JaxRsHandledException extends RuntimeException {
    
    private String i18nKey;
    private String plainMessage;
    private HttpStatus status;
    private List<ErrorResponse.ValidationField> invalidFields;

    public JaxRsHandledException(HttpStatus aStatus, String message, List<ErrorResponse.ValidationField> allInvalidFields) {
        super();
        this.status = aStatus;
        this.invalidFields = allInvalidFields;
    }

    public JaxRsHandledException(HttpStatus aStatus) {
        super();
        this.status = aStatus;
    }

    public String getI18nKey() {
        return this.i18nKey;
    }

    public String getPlainMessage() {
        return (this.plainMessage != null) ? this.plainMessage: super.getMessage();
    }

    public HttpStatus getStatus() {
        return this.status;
    }

    public List<ErrorResponse.ValidationField> getInvalidFields() {
        return invalidFields;
    }

    public JaxRsHandledException i18N(String aI18nKey) {
        this.i18nKey = aI18nKey;
        return this;
    }

    public JaxRsHandledException status(HttpStatus aStatus) {
        this.status = aStatus;
        return this;
    }

    public JaxRsHandledException msg(String aPlainMessage) {
        this.plainMessage = aPlainMessage;
        return this;
    }
    

    public static JaxRsHandledException jaxRsHandled() {
    
        return new JaxRsHandledException(HttpStatus.BAD_REQUEST);
    }
    
    public static JaxRsHandledException jaxRsHandled(HttpStatus aStatus) {
    
        return new JaxRsHandledException(aStatus);
    }

    public static JaxRsHandledException jaxRsHandled(PersistenceException ex) {
 
        if (ex.getCause() instanceof ConstraintViolationException) {
            throw handleConstraintViolation((ConstraintViolationException)ex.getCause());
        }
 
        return new JaxRsHandledException(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static JaxRsHandledException jaxRsHandled(ConstraintViolationException ex) {
 
        return handleConstraintViolation(ex);
    }
    
    private static JaxRsHandledException handleConstraintViolation(ConstraintViolationException ex) {
        
        List<ErrorResponse.ValidationField> invalidFields = new ArrayList<>();
        
        for(ConstraintViolation<?> validation: ex.getConstraintViolations()){
            invalidFields.add(new ErrorResponse.ValidationField(validation));
        }
        String message = ex.getMessage();
        JaxRsHandledException excp = new JaxRsHandledException(HttpStatus.BAD_REQUEST, message, invalidFields);
        return excp;
    }

    
}
