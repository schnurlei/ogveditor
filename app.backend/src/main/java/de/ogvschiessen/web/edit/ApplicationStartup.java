package de.ogvschiessen.web.edit;

import de.ogvschiessen.web.edit.auth.model.ERole;
import de.ogvschiessen.web.edit.auth.model.OgvRole;
import de.ogvschiessen.web.edit.auth.model.OgvUser;
import de.ogvschiessen.web.edit.auth.repository.OgvUserRepository;
import de.ogvschiessen.web.edit.auth.repository.RoleRepository;
import de.ogvschiessen.web.edit.auth.service.AuthController;
import de.ogvschiessen.web.edit.migration.MigrateNews;
import de.ogvschiessen.web.edit.page.model.OgvEvent;
import de.ogvschiessen.web.edit.page.model.OgvImage;
import de.ogvschiessen.web.edit.page.model.OgvPlace;
import de.ogvschiessen.web.edit.page.repo.OgvEventRepository;
import de.ogvschiessen.web.edit.page.repo.OgvImageRepository;
import de.ogvschiessen.web.edit.page.repo.OgvNewsRepository;
import de.ogvschiessen.web.edit.page.repo.OgvPlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger LOG = Logger.getLogger(AuthController.class.getName());

    private RoleRepository roleRepo;
    private PasswordEncoder passwordEncoder;
    @Autowired
    private OgvPlaceRepository placeRepo;

    @Autowired
    private OgvEventRepository eventRepo;

    @Autowired
    private OgvNewsRepository newsRepo;

    @Autowired
    private OgvImageRepository imageRepo;

    @Autowired
    private OgvUserRepository userRepo;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    public ApplicationStartup(RoleRepository roleRepo, PasswordEncoder passwordEncoder) {
        this.roleRepo = roleRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {

        Arrays.asList(ERole.values()).forEach(eRole -> {

            if(this.roleRepo.findByName(eRole).isEmpty()) {
                final OgvRole newRole = new OgvRole(eRole);
                this.roleRepo.save(newRole);
            }
        });

        final OgvPlace place = new OgvPlace();
        place.setLat(new BigDecimal("30.12345"));
        place.setLng(new BigDecimal("80.76543"));
        place.setCity("Hauptr 8; Tussenhausen");
        this.placeRepo.save(place);

        this.eventRepo.save(createOgvEvent("wintergarten", LocalDate.now(), "Gärten im Winte", place));
        this.eventRepo.save(createOgvEvent("mitgliederversammlung", LocalDate.now().minusDays(20), "Mitglieder Versammlung", place));
        this.eventRepo.save(createOgvEvent("event3", LocalDate.now().plusDays(20), "event3_text", place));
        this.eventRepo.save(createOgvEvent("event4", LocalDate.now().minusDays(30), "event4_text1", place));
        this.eventRepo.save(createOgvEvent("event5", LocalDate.now().plusDays(17), "event5_text1", place));

        try {
            final OgvImage ogvImage = new OgvImage();
            ogvImage.setImageName("Chilli.JPG");
            ogvImage.setMimeType("jpg");
            ogvImage.setImgageContent(getClass().getResourceAsStream("chilli.JPG").readAllBytes());
            this.imageRepo.save(ogvImage);
        } catch (IOException e) {
            LOG.log(Level.SEVERE, e.getLocalizedMessage(), e);
        }

        try {
            migrateOgvNews();
        } catch (IOException e) {
            LOG.log(Level.SEVERE, e.getLocalizedMessage(), e);
        }
        createRegisteredUser("rainer");
    }

    private OgvUser createRegisteredUser(final String userName) {

        this.userRepo.deleteAll();
        OgvUser newUser = new OgvUser();
        newUser.setNachname(userName);
        newUser.setEmail(userName + "@example.com");
        newUser.setPassword(this.encoder.encode("ogv-1234"));
        newUser.setRegistrationConfirmed(true);
        newUser.setRoles(Collections.singleton(this.roleRepo.findByName(ERole.ROLE_USER).get()));
        return this.userRepo.save(newUser);
    }

    private void migrateOgvNews() throws IOException {

        new MigrateNews().readNews().stream()
                .forEach( ogvnews -> newsRepo.save(ogvnews) );
    }

    private OgvEvent createOgvEvent(final String id,  final LocalDate date, final String title, final OgvPlace place) {

        String dateString = date.format(DateTimeFormatter.ISO_DATE);
        final OgvEvent ogvEvent = new OgvEvent();
        ogvEvent.setDateStart(date);
        ogvEvent.setId(id + "_" + dateString);
        ogvEvent.setImageSrc("test.jpg");
        ogvEvent.setMarkdownUrl("events/" + id + "-" +dateString);
        ogvEvent.setOrganizer("Organizer1");
        ogvEvent.setPlace(place);
        ogvEvent.setTimeStart(LocalTime.now());
        ogvEvent.setTitle(title);

        return ogvEvent;
    }
}
