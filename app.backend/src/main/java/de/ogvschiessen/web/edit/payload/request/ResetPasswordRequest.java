package de.ogvschiessen.web.edit.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ResetPasswordRequest {

    @NotBlank
    private String resetId;
 
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

    public ResetPasswordRequest() {
    }

    public ResetPasswordRequest(@NotBlank String resetId, @NotBlank @Size(min = 6, max = 40) String password) {
        this.resetId = resetId;
        this.password = password;
    }

    public String getResetId() {
        return resetId;
    }

    public void setResetId(String resetId) {
        this.resetId = resetId;
    }

    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
 }
