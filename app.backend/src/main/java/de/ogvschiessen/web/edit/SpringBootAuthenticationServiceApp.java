package de.ogvschiessen.web.edit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

/**
 * SpringBootAuthenticationServiceApp
 * s. https://bezkoder.com/spring-boot-vue-js-authentication-jwt-spring-security/
 * s. https://ertan-toker.de/spring-boot-spring-security-jwt-token/
 *
 */
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class SpringBootAuthenticationServiceApp
{

     public static void main( String[] args )
    {
        SpringApplication.run(SpringBootAuthenticationServiceApp.class, args);
    }
}
