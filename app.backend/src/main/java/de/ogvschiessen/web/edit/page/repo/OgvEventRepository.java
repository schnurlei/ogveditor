package de.ogvschiessen.web.edit.page.repo;

import de.ogvschiessen.web.edit.page.model.OgvEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface OgvEventRepository extends JpaRepository<OgvEvent, String> {
	
	Optional<OgvEvent> findById(String id);
}
