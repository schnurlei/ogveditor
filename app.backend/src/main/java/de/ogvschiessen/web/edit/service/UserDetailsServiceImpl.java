package de.ogvschiessen.web.edit.service;

import de.ogvschiessen.web.edit.auth.model.OgvUser;
import de.ogvschiessen.web.edit.auth.repository.OgvUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    OgvUserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        OgvUser user = userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + email));
        if(user.isRegistrationConfirmed()) {
            return UserDetailsImpl.build(user);
        } else {
            throw new UsernameNotFoundException("User Registration not confirmed! Username: " + email);
        }

    }

}
