package de.ogvschiessen.web.edit.page.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class OgvImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max=255)
    private String imageName;

    @Size(max=50)
    private String MimeType;

    @Lob
    @Basic(fetch=FetchType.LAZY)
    private byte[] imgageContent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getMimeType() {
        return MimeType;
    }

    public void setMimeType(String mimeType) {
        MimeType = mimeType;
    }

    public byte[] getImgageContent() {
        return imgageContent;
    }

    public void setImgageContent(byte[] imgageContent) {
        this.imgageContent = imgageContent;
    }
}
