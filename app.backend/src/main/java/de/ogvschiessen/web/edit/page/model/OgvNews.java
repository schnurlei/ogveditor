package de.ogvschiessen.web.edit.page.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
public class OgvNews {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Size(max=50)
    private String id;

    @Size(max=120)
    @Column(length=120)
    @NotNull
    private String subject;

    @Size(max=120)
    @Column(length=120)
    @NotNull
    private String title;

    @Size(max=120)
    @Column(length=120)
    @NotNull
    private String abstractText;

    @NotNull
    private LocalDate newsDate;

    @NotNull
    private LocalTime newsTime;

    @Size(max=255)
    private String imageSrc;

    @Size(max=255)
    private String markdownUrl;

    @Size(max=255)
    private String link;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbstractText() {
        return abstractText;
    }

    public void setAbstractText(String abstractText) {
        this.abstractText = abstractText;
    }

    public LocalDate getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(LocalDate newsDate) {
        this.newsDate = newsDate;
    }

    public LocalTime getNewsTime() {
        return newsTime;
    }

    public void setNewsTime(LocalTime newsTime) {
        this.newsTime = newsTime;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public String getMarkdownUrl() {
        return markdownUrl;
    }

    public void setMarkdownUrl(String markdownUrl) {
        this.markdownUrl = markdownUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
