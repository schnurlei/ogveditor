package de.ogvschiessen.web.edit.page;

import de.ogvschiessen.web.edit.page.model.OgvEvent;
import de.ogvschiessen.web.edit.page.model.OgvImage;
import de.ogvschiessen.web.edit.page.model.OgvPlace;
import de.ogvschiessen.web.edit.page.repo.OgvEventRepository;
import de.ogvschiessen.web.edit.page.repo.OgvPlaceRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestPageObjects {
    
    private static final Logger LOG = Logger.getLogger(TestPageObjects.class.getName());
    
    @Autowired
    private OgvPlaceRepository placeRepo;

    @Autowired
    private OgvEventRepository eventRepo;

    @Autowired
    private TestRestTemplate restTemplate;
    
    @Test
    public void testReadMetaDataJdy() throws IOException {
    
        String result = this.restTemplate.getForObject("/api/page/jdy/meta", String.class);
    
    }
    
    @Test
    public void testReadEventsJdy() throws IOException {
        
        String result = this.restTemplate.getForObject("/api/page/jdy/data/OgvEvent", String.class);
        
    }
    
    @Test
    public void testCrudImage() throws IOException {
    
        final OgvImage newImage = this.createOgvImageJpg("chilli.JPG");
    
        final URI imageUrl = this.restTemplate.postForLocation("/api/page/ogvimage", newImage, Object.class);
        OgvImage readImage = this.restTemplate.getForObject(imageUrl, OgvImage.class);
        readImage.setImageName("changed image name");
        this.restTemplate.put("/api/page/ogvimage", readImage, OgvImage.class);
        readImage = this.restTemplate.getForObject(imageUrl, OgvImage.class);
        assertThat(readImage.getImageName()).isEqualTo ("changed image name");
        this.restTemplate.delete(imageUrl);
        readImage = this.restTemplate.getForObject(imageUrl, OgvImage.class);
        assertThat(readImage.getId()).isNull();
    }
    
    @Test
    public void testCrudEvent() throws IOException {
    
        final OgvPlace place = this.createOgvPlace("30.12345", "80.76543", "Hauptr 8; Tussenhausen");
        final OgvPlace createdPlace = this.placeRepo.save(place);

        final OgvEvent event = this.createOgvEvent("FirstEvent", LocalDate.now(), "Test Event 1", createdPlace);
        this.eventRepo.save(event);

        final OgvEvent ogvEvent = this.restTemplate.getForObject("/api/page/ogvevent/" + event.getId(), OgvEvent.class);
        assertThat(ogvEvent.getTitle()).isEqualTo ("Test Event 1");

        final OgvPlace[] ogvPlaces = this.restTemplate.getForObject("/api/page/allPlaces", OgvPlace[].class);
        assertThat(ogvPlaces[0].getCity()).isEqualTo ("Hauptr 8; Tussenhausen");

        final OgvEvent newEvent = this.createOgvEvent("1234567", LocalDate.now(), "New_CreatedObj", createdPlace);

        final OgvEvent createdEvent = this.restTemplate.postForObject("/api/page/ogvevent", newEvent, OgvEvent.class);
        assertThat(createdEvent.getTitle()).isEqualTo ("New_CreatedObj");
        assertThat(createdEvent.getPlace().getCity()).isEqualTo ("Hauptr 8; Tussenhausen");

        createdEvent.setTitle("Updated title 123455");
        this.restTemplate.put("/api/page/ogvevent", createdEvent, OgvEvent.class);
        final OgvEvent updatedEvent = this.restTemplate.getForObject("/api/page/ogvevent/" + createdEvent.getId(), OgvEvent.class);
        assertThat(updatedEvent.getTitle()).isEqualTo ("Updated title 123455");
        assertThat(updatedEvent.getPlace().getCity()).isEqualTo ("Hauptr 8; Tussenhausen");
    }
    
    private OgvImage createOgvImageJpg(String resourceName) throws IOException {
        final OgvImage ogvImage = new OgvImage();
        ogvImage.setImageName(resourceName);
        ogvImage.setMimeType("jpg");
        ogvImage.setImgageContent(getClass().getResourceAsStream(resourceName).readAllBytes());
        return ogvImage;
    }
    
    private OgvPlace createOgvPlace(String lat, String lng, String city) {
        final OgvPlace place = new OgvPlace();
        place.setLat(new BigDecimal(lat));
        place.setLng(new BigDecimal(lng));
        place.setCity(city);
        return place;
    }
    
    private OgvEvent createOgvEvent(final String id,  final LocalDate date, final String title, final OgvPlace place) {
        
        String dateString = date.format(DateTimeFormatter.ISO_DATE);
        final OgvEvent ogvEvent = new OgvEvent();
        ogvEvent.setDateStart(date);
        ogvEvent.setId(id + "_" + dateString);
        ogvEvent.setImageSrc("test.jpg");
        ogvEvent.setMarkdownUrl("events/" + id + "-" +dateString);
        ogvEvent.setOrganizer("Organizer1");
        ogvEvent.setPlace(place);
        ogvEvent.setTimeStart(LocalTime.now());
        ogvEvent.setTitle(title);
        
        return ogvEvent;
    }
}
