package de.ogvschiessen.web.edit.service;

import de.ogvschiessen.web.edit.auth.model.ERole;
import de.ogvschiessen.web.edit.auth.model.OgvRole;
import de.ogvschiessen.web.edit.auth.model.OgvUser;
import de.ogvschiessen.web.edit.auth.repository.OgvUserRepository;
import de.ogvschiessen.web.edit.auth.repository.RoleRepository;
import de.ogvschiessen.web.edit.payload.request.LoginRequest;
import de.ogvschiessen.web.edit.payload.request.ResetPasswordRequest;
import de.ogvschiessen.web.edit.payload.request.SignupRequest;
import de.ogvschiessen.web.edit.payload.response.JwtResponse;
import de.ogvschiessen.web.edit.payload.response.MessageResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestLoginController {

	public static final String DEFAULT_PASSWORD = "ior340afuzjfa";
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private RoleRepository roleRepo;
	
	@Autowired
	private OgvUserRepository userRepo;

	@Autowired
	PasswordEncoder encoder;

	@Test
	public void testAllAccess()  {
		assertThat(this.restTemplate.getForObject("/api/test/all",	String.class)).contains("Public Content.");
	}
	
	@Test
	public void testAdminAccessUnauthorized() {
		assertThat(this.restTemplate.getForEntity("/api/test/admin",String.class).getStatusCode())
				.isEqualTo (HttpStatus.UNAUTHORIZED);
	}
	
	@Test
	public void testSignupNoRoles() {

		this.userRepo.deleteAll();
		this.roleRepo.deleteAll();
		final SignupRequest request = this.createSignupRequest("user1");
		final ResponseEntity<MessageResponse> response = this.restTemplate.postForEntity("/api/auth/signup", request, MessageResponse.class);
		assertThat(response.getStatusCode()).isEqualTo (HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
	@Test
	public void testSignup() {
		
		this.createDefaultRoles();
		final SignupRequest request = this.createSignupRequest("user1");
		final MessageResponse response = this.restTemplate.postForObject("/api/auth/signup", request, MessageResponse.class);
		assertThat(response.getMessage()).isEqualTo ("User registered successfully!");
		
	}
	
	@Test
	public void testSignIn() {
		
		this.createDefaultRoles();
		final SignupRequest request = 	this.createSignupRequest("user1");
		final MessageResponse response = this.restTemplate.postForObject("/api/auth/signup", request, MessageResponse.class);
		assertThat(response.getMessage()).isEqualTo ("User registered successfully!");

		OgvUser createdUser = this.userRepo.findByEmail("user1@example.com").get();
		String confirmUrl = "/api/auth/confirmRegistration/"+ createdUser.getRegistrationId();
		ResponseEntity<String> confirmResponse = this.restTemplate.exchange(confirmUrl, HttpMethod.PUT, HttpEntity.EMPTY, String.class);
		assertThat(confirmResponse.getStatusCode()).isEqualTo (HttpStatus.OK);

		final LoginRequest login = new LoginRequest();
		login.setEmail(request.getEmail());
		login.setPassword(DEFAULT_PASSWORD);
		final JwtResponse loginResponse = this.restTemplate.postForObject("/api/auth/signin", login, JwtResponse.class);
		assertThat(loginResponse.getUsername()).isEqualTo (request.getEmail());
		assertThat(loginResponse.getNachname()).isEqualTo ("user1");
		assertThat(loginResponse.getTokenType()).isEqualTo ("Bearer");
	}
	
	@Test
	public void testUserAccessAuthorized() {

		OgvUser user = createRegisteredUser("user1");
		
		final LoginRequest login = new LoginRequest();
		login.setEmail(user.getEmail());
		login.setPassword(DEFAULT_PASSWORD);
		final JwtResponse loginResponse = this.restTemplate.postForObject("/api/auth/signin", login, JwtResponse.class);
		assertThat(loginResponse.getUsername()).isEqualTo ("user1@example.com");
		assertThat(loginResponse.getNachname()).isEqualTo ("user1");
		assertThat(loginResponse.getTokenType()).isEqualTo ("Bearer");
		
		final HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", loginResponse.getTokenType() + " " + loginResponse.getAccessToken());
		
		assertThat(this.restTemplate.exchange("/api/test/user", HttpMethod.GET, new HttpEntity<>(headers),
				String.class).getStatusCode()).isEqualTo (HttpStatus.OK);
		
	}
	
	@Test
	public void testSignInInvalidPassword() {

		this.createDefaultRoles();
		final SignupRequest request = 	this.createSignupRequest("user1");
		final MessageResponse response = this.restTemplate.postForObject("/api/auth/signup", request, MessageResponse.class);
		assertThat(response.getMessage()).isEqualTo ("User registered successfully!");
		
		final LoginRequest login = new LoginRequest();
		login.setEmail(request.getEmail());
		login.setPassword("ior340afuzj");

		final ResponseEntity<String> loginResponse = this.restTemplate.postForEntity("/api/auth/signin", login, String.class);
		assertThat(loginResponse.getStatusCode()).isEqualTo (HttpStatus.UNAUTHORIZED);
	}

	@Test
	public void testResetPassword() {

		final String resetId = "ajflasjfalsjfk123";
		final String newPassword = "u74f039sdl";
		this.createDefaultRoles();
		OgvUser user = createRegisteredUser("user1");
		user.setPasswordResetId(resetId);
		this.userRepo.save(user);
		String url = "/api/auth/resetPassword/";
		HttpEntity<ResetPasswordRequest> request = new HttpEntity<>(new ResetPasswordRequest(resetId, newPassword));
		ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
		assertThat(response.getStatusCode()).isEqualTo (HttpStatus.OK);
		OgvUser updatedUser = this.userRepo.findById(user.getId()).get();
		assertThat(updatedUser.getPasswordResetId()).isNull();
		assertThat(updatedUser.getPassword()).isNotEmpty();
	}

	@Test
	public void testResetPasswordInvalidResetId() {

		final String resetId = "ajflasjfalsjfk123";
		final String newPassword = "u74f039sdl";
		this.createDefaultRoles();
		OgvUser user = createRegisteredUser("user1");
		user.setPasswordResetId(resetId);
		user.setPassword("<Invalid>");
		this.userRepo.save(user);
		String url = "/api/auth/resetPassword/";
		HttpEntity<ResetPasswordRequest> request = new HttpEntity<>(new ResetPasswordRequest(resetId+"invalid", newPassword));
		ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
		assertThat(response.getStatusCode()).isEqualTo (HttpStatus.NOT_FOUND);
		OgvUser updatedUser = this.userRepo.findById(user.getId()).get();
		assertThat(updatedUser.getPasswordResetId()).isNotEmpty();
		assertThat(updatedUser.getPassword()).isEqualTo("<Invalid>");
	}

	@Test
	public void testResetPasswordInvalidNewPassword() {

		final String resetId = "ajflasjfalsjfk123";
		final String newPassword = "1";
		this.createDefaultRoles();
		OgvUser user = createRegisteredUser("user1");
		user.setPasswordResetId(resetId);
		user.setPassword("<Invalid>");
		this.userRepo.save(user);
		String url = "/api/auth/resetPassword/";
		HttpEntity<ResetPasswordRequest> request = new HttpEntity<>(new ResetPasswordRequest(resetId, newPassword));
		ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
		assertThat(response.getStatusCode()).isEqualTo (HttpStatus.BAD_REQUEST);
		OgvUser updatedUser = this.userRepo.findById(user.getId()).get();
		assertThat(updatedUser.getPasswordResetId()).isNotEmpty();
		assertThat(updatedUser.getPassword()).isEqualTo("<Invalid>");
	}

	@Test
	public void testRequestPasswordReset() {

		this.createDefaultRoles();
		OgvUser user = createRegisteredUser("user1");
		String url = "/api/auth/requestPassword/"+ user.getEmail();
		ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.PUT, HttpEntity.EMPTY, String.class);
		assertThat(response.getStatusCode()).isEqualTo (HttpStatus.OK);
		OgvUser updatedUser = this.userRepo.findById(user.getId()).get();
		assertThat(updatedUser.getPasswordResetId()).isNotEmpty();
	}

	@Test
	public void testRequestPasswordInvalidUser() {

		this.createDefaultRoles();
		final OgvUser user = createRegisteredUser("user1");
		final String url = "/api/auth/requestPassword/"+ user.getEmail() + "invalid";
		final ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.PUT, HttpEntity.EMPTY, String.class);
		assertThat(response.getStatusCode()).isEqualTo (HttpStatus.NOT_FOUND);
	}

	private SignupRequest createSignupRequest(final String userName) {
		
		final SignupRequest request = new SignupRequest();
		request.setNachname(userName);
		request.setEmail(userName + "@example.com");
		request.setPassword(DEFAULT_PASSWORD);
		return request;
	}

	private OgvUser createRegisteredUser(final String userName) {

		this.userRepo.deleteAll();
		OgvUser newUser = new OgvUser();
		newUser.setNachname(userName);
		newUser.setEmail(userName + "@example.com");
		newUser.setPassword(this.encoder.encode(DEFAULT_PASSWORD));
		newUser.setRegistrationConfirmed(true);
		newUser.setRoles(Collections.singleton(this.roleRepo.findByName(ERole.ROLE_USER).get()));
		return this.userRepo.save(newUser);
	}

	private void createDefaultRoles() {
		
		this.userRepo.deleteAll();
		this.roleRepo.deleteAll();
		final OgvRole newRole = new OgvRole(ERole.ROLE_USER);
		this.roleRepo.save(newRole);
	}
}
